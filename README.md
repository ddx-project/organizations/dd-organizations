# [DD-Protocol Dashboard]

DD-Protocol was created from ddx-react that was created using [create-react-app](https://github.com/facebook/create-react-app) and it uses the [Material UI](https://github.com/mui-org/material-ui) framework inspired by Google's Material Design.

Pull this repo, navigate to its directory and:

- `npm ci`

## Production

To create a production bundle:

- `npm run build`


## Development

To work in development:

- `npm run start`

## Licensing

- Licensed under MIT
